using System;
using AdventurePlanner;
using Xunit;

namespace Test
{
    public class SeasonTest
    {
        [Theory]
        [InlineData(1, Season.Winter)]
        [InlineData(2, Season.Winter)]
        [InlineData(3, Season.Winter)]
        [InlineData(4, Season.Spring)]
        [InlineData(5, Season.Spring)]
        [InlineData(6, Season.Spring)]
        [InlineData(7, Season.Summer)]
        [InlineData(8, Season.Summer)]
        [InlineData(9, Season.Summer)]
        [InlineData(10, Season.Autumn)]
        [InlineData(11, Season.Autumn)]
        [InlineData(12, Season.Autumn)]
        public void SeasonConverterTest(int month, Season season)
        {
            Assert.Equal(season, SeasonData.GetSeason(DateTime.Parse("2021-"+month+"-15")));
        }    
    }
}

