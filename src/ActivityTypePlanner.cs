using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventurePlanner
{
    public class ActivityTypePlanner
    {
        private List<Activity> activities;
        
        public ActivityTypePlanner()
        {
            activities = Activity.LoadAllActivities();
        }

        public void Start()
        {
            Console.WriteLine();
            ConsoleUtils.Print("### Activity Type Planner ###");

            DateTime now = DateTime.Now;
            
            Console.WriteLine(now.ToString("MMMM dd, yyyy, h:mmtt"));

            Location leavenworth = new Location("Leavenworth");
            Conditions conditions = new Conditions(leavenworth);
            
            // todo: create interface for cached data classes (refreshed bool)
            // todo: create interface for the expected functions (print status)
            // todo: move this printing to a view class
            if (conditions.CurrentWeather.Refreshed)
            {
                ConsoleUtils.Print("Downloaded latest weather forecast");
            }
            else
            {
                ConsoleUtils.Print("Loaded weather forecast from cache");
            }
            using (new ConsoleWriteIndent())
                conditions.CurrentWeather.PrintBasics();
            
            
            if (conditions.AvalancheForecast.Refreshed)
            {
                ConsoleUtils.Print("Downloaded latest avalanche forecast");
            }
            else
            {
                ConsoleUtils.Print("Loaded avalanche forecast from cache");
            }
            using (new ConsoleWriteIndent())
                conditions.AvalancheForecast.PrintReport();
            
            
            if (conditions.Resort.Refreshed)
            {
                ConsoleUtils.Print("Downloaded latest resort data");
            }
            else
            {
                ConsoleUtils.Print("Loaded resort data from cache");
            }
            using (new ConsoleWriteIndent())
                conditions.Resort.PrintStatus();
            
            if (conditions.MountainPassData.Refreshed)
            {
                ConsoleUtils.Print("Downloaded latest mountain pass conditions");
            }
            else
            {
                ConsoleUtils.Print("Loaded mountain pass conditions from cache");
            }
            using (new ConsoleWriteIndent())
                conditions.MountainPassData.PrintStatus();


            PrintWeatherCheck(conditions);
        }

        public void PrintWeatherCheck(Conditions conditions)
        {
            Console.WriteLine();
            ConsoleUtils.Print($"Weather Check");

            using (new ConsoleWriteIndent())
            {
                foreach (Activity a in activities.ToList())
                {
                    bool possible = a.WeatherCheck(conditions, out string reason);
                    if (possible)
                    {
                        using (new TempTextColor(ConsoleColor.Green))
                        {
                            ConsoleUtils.Print(a.Name + " passes weather check");
                        }
                    }
                    else
                    {
                        using (new TempTextColor(ConsoleColor.Red))
                        {
                            ConsoleUtils.Print(a.Name + " fails weather check");
                        }
                        activities.Remove(a);
                    }
                    
                    using (new ConsoleWriteIndent())
                    {
                        ConsoleUtils.Print(reason);
                    }
                }
            }
        }
    }
}