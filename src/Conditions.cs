using AdventurePlanner.Avalanche;
using AdventurePlanner.Weather;

namespace AdventurePlanner
{
    public class Conditions
    {
        public CurrentWeather CurrentWeather;
        public AvalancheForecast AvalancheForecast;
        public Resort Resort;
        public MountainPass MountainPassData;

        public Conditions(CurrentWeather weather, AvalancheForecast avy, Resort resort, MountainPass mountainPass)
        {
            CurrentWeather = weather;
            AvalancheForecast = avy;
            Resort = resort;
            MountainPassData = mountainPass;
        }

        public Conditions(Location location)
        {
            IWeatherAPI api = new OpenWeather();
            CurrentWeather = api.GetCurrentConditions(location);
            
            AvalancheForecast = AvalancheForecast.GetCurrentForecast(out bool refreshed);

            Resort = new Resort();

            MountainPassData = MountainPass.Load();
        }
    }
}