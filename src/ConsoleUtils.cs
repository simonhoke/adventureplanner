using System;

namespace AdventurePlanner
{
    public class ConsoleUtils
    {
        public static int IndentLevel;
        
        public static void Print(string text)
        {
            for (int i=0; i<IndentLevel; i++)
                Console.Write("    ");
            
            Console.WriteLine(text);
        }
    }
    
    public class TempTextColor : IDisposable
    {
        private ConsoleColor oldColor;
        public TempTextColor(ConsoleColor color)
        {
            oldColor = Console.ForegroundColor;
            Console.ForegroundColor = color;
        }

        public void Dispose() { Console.ForegroundColor = oldColor; }
    }
    
    public class ConsoleWriteIndent : IDisposable
    {
        public ConsoleWriteIndent() { ConsoleUtils.IndentLevel++; }
        public void Dispose() { ConsoleUtils.IndentLevel--; }
    }
}