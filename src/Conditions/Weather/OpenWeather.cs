using System;
using Newtonsoft.Json;

namespace AdventurePlanner.Weather
{
    public class OpenWeather : IWeatherAPI
    {
        private string ApiKey;
        private string BaseUrl = "https://api.openweathermap.org/data/2.5/weather";

        public OpenWeather()
        {
            ApiKey = Environment.GetEnvironmentVariable("openweatherapikey");

            if (ApiKey == null)
                throw new Exception("Missing OpenWeather API Key");
        }

        private string GetAPIResponse(string cityName, string stateCode, out bool refreshed)
        {
            string url = $"{BaseUrl}?q={cityName},{stateCode}&appid={ApiKey}";
            
            string result = CachedResourceLoader.GetURL(url, out bool didRefresh);
            refreshed = didRefresh;
            return result;
        }

        private string GetAPIResponse(int cityCode, out bool refreshed)
        {
            string url = $"{BaseUrl}?id={cityCode}&appid={ApiKey}";
            
            string result = CachedResourceLoader.GetURL(url, out bool didRefresh);
            refreshed = didRefresh;
            return result;
        }
        
        public CurrentWeather GetCurrentConditions(Location location)
        {
            string weatherResponse = GetAPIResponse(location.OpenWeatherCityCode, out bool didRefresh);
            
            CurrentWeather cc = JsonConvert.DeserializeObject<CurrentWeather>(weatherResponse);
            cc.Refreshed = didRefresh;

            return cc;
        }
    }
}