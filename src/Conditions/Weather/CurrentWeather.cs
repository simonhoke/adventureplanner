using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace AdventurePlanner.Weather
{
    public class CurrentWeather
    {
        // true if just grabbed, false if from cache
        public bool Refreshed;
        
        [JsonProperty("coord")] public Coord Coord { get; set; }

        [JsonProperty("weather")] public List<Desc> Desc { get; set; }

        [JsonProperty("base")] public string Base { get; set; }

        [JsonProperty("main")] public Main Main { get; set; }

        [JsonProperty("visibility")] public long Visibility { get; set; }

        [JsonProperty("wind")] public Wind Wind { get; set; }

        [JsonProperty("clouds")] public Clouds Clouds { get; set; }

        [JsonProperty("dt")] public long Dt { get; set; }

        [JsonProperty("sys")] public Sys Sys { get; set; }

        [JsonProperty("timezone")] public long Timezone { get; set; }

        [JsonProperty("id")] public long Id { get; set; }

        [JsonProperty("name")] public string Name { get; set; }

        [JsonProperty("cod")] public long Cod { get; set; }

        /// <summary>
        /// https://openweathermap.org/weather-conditions
        /// </summary>
        /// <returns></returns>
        public bool IsDry()
        {
            // no storm or active rain and cloud cover is between 0 and 50%
            return Desc[0].Id > 800 && Desc[0].Id <= 802;
        }

        public void PrintBasics()
        {
            Console.WriteLine($"    Current conditions in {Name}: ");
            Console.WriteLine("         Desc: " + Desc[0].Description);
            Console.WriteLine("         Temp: " + UnitConverter.KToF_Pretty(Main.Temp));
            Console.WriteLine("         Wind: " + UnitConverter.MsToMph_Pretty(Wind.Speed));
        }

        public double GetTempF()
        {
            return UnitConverter.KToF(Main.Temp);
        }
    }

    public class Clouds
    {
        [JsonProperty("all")] public long All { get; set; }
    }

    public class Coord
    {
        [JsonProperty("lon")] public double Lon { get; set; }

        [JsonProperty("lat")] public double Lat { get; set; }
    }

    public class Main
    {
        [JsonProperty("temp")] public double Temp { get; set; }

        [JsonProperty("feels_like")] public double FeelsLike { get; set; }

        [JsonProperty("temp_min")] public double TempMin { get; set; }

        [JsonProperty("temp_max")] public double TempMax { get; set; }

        [JsonProperty("pressure")] public long Pressure { get; set; }

        [JsonProperty("humidity")] public long Humidity { get; set; }
    }

    public class Sys
    {
        [JsonProperty("type")] public long Type { get; set; }

        [JsonProperty("id")] public long Id { get; set; }

        [JsonProperty("country")] public string Country { get; set; }

        [JsonProperty("sunrise")] public long Sunrise { get; set; }

        [JsonProperty("sunset")] public long Sunset { get; set; }
    }

    public class Desc
    {
        [JsonProperty("id")] public long Id { get; set; }

        [JsonProperty("main")] public string Main { get; set; }

        [JsonProperty("description")] public string Description { get; set; }

        [JsonProperty("icon")] public string Icon { get; set; }
    }

    public class Wind
    {
        [JsonProperty("speed")] public double Speed { get; set; }

        [JsonProperty("deg")] public long Deg { get; set; }

        [JsonProperty("gust")] public double Gust { get; set; }
    }
}