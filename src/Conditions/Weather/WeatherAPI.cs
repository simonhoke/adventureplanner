namespace AdventurePlanner.Weather
{
    public interface IWeatherAPI
    {
        CurrentWeather GetCurrentConditions(Location location);
    }
}