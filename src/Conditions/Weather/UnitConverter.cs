using System;

namespace AdventurePlanner.Weather
{
    public class UnitConverter
    {
        public static double KToC(double kelvinTemp)
        {
            return kelvinTemp - 273.15;
        }

        public static double CtoF(double celsiusTemp)
        {
            return 1.8 * celsiusTemp + 32;
        } 

        public static double KToF(double kelvinTemp)
        {
            return CtoF(KToC(kelvinTemp));
        }
        
        public static string KToF_Pretty(double kelvinTemp)
        {
            double result = CtoF(KToC(kelvinTemp));
            return Math.Round(result, 1) + "F";
        }

        public static double MsToMph(double metersPerSecond)
        {
            return metersPerSecond * 2.237;
        }
        
        public static string MsToMph_Pretty(double metersPerSecond)
        {
            return Math.Round(MsToMph(metersPerSecond), 1) + "mph";
        }
    }
}