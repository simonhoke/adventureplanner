using System.IO;

namespace AdventurePlanner.Avalanche
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    /// <summary>
    /// https://avalanche.org/avalanche-encyclopedia/danger-scale/
    /// Unknown added because enums are 0-indexed and the danger scale is 1-indexed
    /// </summary>
    public enum DangerLevel
    {
        Unknown,
        Low,
        Moderate,
        Considerable,
        High,
        Extreme
    }

    public partial class AvalancheForecast
    {
        private static readonly string avalancheForecastDataSourceUrl = Environment.GetEnvironmentVariable("avalancheForecastDataSourceUrl");
        
        // true if just grabbed, false if from cache
        public bool Refreshed;

        static AvalancheForecast()
        {
            if (avalancheForecastDataSourceUrl == null)
                throw new Exception("Missing Avalanche Forecast Data Source URL");
        }
        
        public static AvalancheForecast GetCurrentForecast(out bool refreshed, bool useTestData = false)
        {
            string json;

            if (useTestData)
            {
                json = File.ReadAllText("../../../../test/NWACproduct.json");
                ConsoleUtils.Print("NOTICE: Using Dummy Avalanche Data");
                refreshed = false;
            }
            else
            {
                json = CachedResourceLoader.GetURL(avalancheForecastDataSourceUrl, out bool didRefresh);
                refreshed = didRefresh;
            }
                
            return FromJson(json);
        }

        public string DangerName(long level)
        {
            return DangerLevel.GetName((DangerLevel) level);
        }

        public void PrintReport()
        {

            if (Danger.Count < 1)
            {
                ConsoleUtils.Print("Report does not include danger level.");
                ConsoleUtils.Print("Bottom Line: " + GetBottomLine());
                return;
            }
            
            // TODO: PrintPyramids();
            
            // TODO: color the numbers
            // TODO: more rigorous checking of today vs tomorrow
            ConsoleUtils.Print($"Danger level today is {DangerName(Danger[0].Lower)} below treeline, {DangerName(Danger[0].Middle)} near treeline, {DangerName(Danger[0].Upper)} above treeline");
            ConsoleUtils.Print($"Danger level tomorrow is {DangerName(Danger[1].Lower)} below treeline, {DangerName(Danger[1].Lower)} near treeline, {DangerName(Danger[1].Lower)} above treeline");
            
            ConsoleUtils.Print("Bottom Line: " + GetBottomLine());
            
        }
        
        
        [JsonProperty("id")] public long Id { get; set; }

        [JsonProperty("published_time")] public DateTimeOffset PublishedTime { get; set; }

        [JsonProperty("expires_time")] public DateTimeOffset ExpiresTime { get; set; }

        [JsonProperty("created_at")] public DateTimeOffset CreatedAt { get; set; }

        [JsonProperty("updated_at")] public DateTimeOffset UpdatedAt { get; set; }

        [JsonProperty("author")] public string Author { get; set; }

        [JsonProperty("product_type")] public string ProductType { get; set; }

        [JsonProperty("bottom_line")] public string BottomLine { get; set; }

        public string GetBottomLine()
        {
            string statement = BottomLine;
            
            if (statement == null)
                statement = HazardDiscussion;

            return HtmlUtilities.ConvertToPlainText(statement);
        }

        [JsonProperty("hazard_discussion")] public string HazardDiscussion { get; set; }

        [JsonProperty("weather_discussion")] public object WeatherDiscussion { get; set; }

        [JsonProperty("announcement")] public object Announcement { get; set; }

        [JsonProperty("status")] public string Status { get; set; }

        [JsonProperty("media")] public List<Media> Media { get; set; }

        [JsonProperty("weather_data")] public object WeatherData { get; set; }

        [JsonProperty("json_data")] public object JsonData { get; set; }

        [JsonProperty("avalanche_center")] public AvalancheCenter AvalancheCenter { get; set; }

        [JsonProperty("forecast_avalanche_problems")]
        public List<ForecastAvalancheProblem> ForecastAvalancheProblems { get; set; }

        [JsonProperty("danger")] public List<Danger> Danger { get; set; }

        private Danger GetCurrentDanger()
        {
            foreach (Danger d in Danger)
            {
                if (d.ValidDay == "current")
                    return d;
            }

            return null;
        }
        
        private Danger GetTomorrowDanger()
        {
            foreach (Danger d in Danger)
            {
                if (d.ValidDay == "tomorrow")
                    return d;
            }

            return null;
        }
        
        /// <summary>
        /// See avalanche.org for more information on the 1 (low) to 5 (extreme) avalanche danger scale
        /// </summary>
        /// <returns></returns>
        public DangerLevel GetHighestDanger()
        {
            Danger today = GetCurrentDanger();
            Danger tomorrow = GetTomorrowDanger();
            
            if (today == null || tomorrow == null)
            {
                ConsoleUtils.Print("Avalanche Forecast Data Error: could not find danger levels");
                return DangerLevel.Extreme; // better safe than sorry
            }

            DangerLevel maxToday = (DangerLevel) Math.Max(today.Lower, Math.Max(today.Middle, today.Upper));
            DangerLevel maxTomorrow = (DangerLevel) Math.Max(tomorrow.Lower, Math.Max(tomorrow.Middle, tomorrow.Upper));

            return (DangerLevel) Math.Max((int) maxToday, (int) maxTomorrow);
        }

        public string GetCurrentDangerCode()
        {
            Danger today = GetCurrentDanger();
            return $"[{today.Lower}{today.Middle}{today.Upper}]";
        }

        [JsonProperty("forecast_zone")] public List<ForecastZone> ForecastZone { get; set; }
    }

    public partial class AvalancheCenter
    {
        [JsonProperty("id")] public string Id { get; set; }

        [JsonProperty("name")] public string Name { get; set; }

        [JsonProperty("url")] public Uri Url { get; set; }

        [JsonProperty("city")] public string City { get; set; }

        [JsonProperty("state")] public string State { get; set; }
    }

    public partial class Danger
    {
        [JsonProperty("lower")] public long Lower { get; set; }

        [JsonProperty("upper")] public long Upper { get; set; }

        [JsonProperty("middle")] public long Middle { get; set; }

        [JsonProperty("valid_day")] public string ValidDay { get; set; }
    }

    public partial class ForecastAvalancheProblem
    {
        [JsonProperty("id")] public long Id { get; set; }

        [JsonProperty("forecast_id")] public long ForecastId { get; set; }

        [JsonProperty("avalanche_problem_id")] public long AvalancheProblemId { get; set; }

        [JsonProperty("rank")] public long Rank { get; set; }

        [JsonProperty("likelihood")] public string Likelihood { get; set; }

        [JsonProperty("discussion")] public string Discussion { get; set; }

        [JsonProperty("media")] public Media Media { get; set; }

        [JsonProperty("location")] public List<string> Location { get; set; }

        [JsonProperty("size")]
        //[JsonConverter(typeof(DecodeArrayConverter))]
        public List<string> Size { get; set; }

        [JsonProperty("name")] public string Name { get; set; }

        [JsonProperty("problem_description")] public string ProblemDescription { get; set; }

        [JsonProperty("icon")] public Uri Icon { get; set; }
    }

    public partial class Media
    {
        [JsonProperty("url")] public Url Url { get; set; }

        [JsonProperty("type")] public string Type { get; set; }

        [JsonProperty("caption")] public string Caption { get; set; }

        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public long? Id { get; set; }
    }

    public partial class Url
    {
        [JsonProperty("large")] public Uri Large { get; set; }

        [JsonProperty("medium")] public Uri Medium { get; set; }

        [JsonProperty("original")] public Uri Original { get; set; }

        [JsonProperty("thumbnail")] public Uri Thumbnail { get; set; }
    }

    public class ForecastZone
    {
        [JsonProperty("id")] public long Id { get; set; }

        [JsonProperty("name")] public string Name { get; set; }

        [JsonProperty("url")] public Uri Url { get; set; }

        [JsonProperty("state")] public string State { get; set; }

        [JsonProperty("zone_id")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long ZoneId { get; set; }
    }

    public partial class AvalancheForecast
    {
        public static AvalancheForecast FromJson(string json) =>
            JsonConvert.DeserializeObject<AvalancheForecast>(json, Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this AvalancheForecast self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter {DateTimeStyles = DateTimeStyles.AssumeUniversal}
            },
        };
    }

    internal class DecodeArrayConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(List<long>);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            reader.Read();
            var value = new List<long>();
            while (reader.TokenType != JsonToken.EndArray)
            {
                var converter = new ParseStringConverter();
                var arrayItem = (long) converter.ReadJson(reader, typeof(long), null, serializer);
                value.Add(arrayItem);
                reader.Read();
            }

            return value;
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            var value = (List<long>) untypedValue;
            writer.WriteStartArray();
            foreach (var arrayItem in value)
            {
                var converter = new ParseStringConverter();
                converter.WriteJson(writer, arrayItem, serializer);
            }

            writer.WriteEndArray();
        }

        public static readonly DecodeArrayConverter Singleton = new DecodeArrayConverter();
    }

    internal class ParseStringConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(long) || t == typeof(long?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            long l;
            if (Int64.TryParse(value, out l))
            {
                return l;
            }

            throw new Exception("Cannot unmarshal type long");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }

            var value = (long) untypedValue;
            serializer.Serialize(writer, value.ToString());
        }
    }
}
       