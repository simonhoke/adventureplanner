using System;

namespace AdventurePlanner
{
    public class Resort
    {
        private readonly string ResortBaseUrl = "https://www.stevenspass.com/";
        
        private readonly string WeatherReportPath = "the-mountain/mountain-conditions/weather-report.aspx";
        private readonly string LiftAndTerrainStatusPath = "the-mountain/mountain-conditions/lift-and-terrain-status.aspx";

        private string weatherReportSource;
        private string liftStatusSource;

        public readonly string OverallSnowConditions;
        public readonly string LastUpdated;
        
        public readonly double OvernightSnowfall;
        public readonly double TwentyFourHourSnowAmount;
        public readonly double FourtyEightHourSnowAmount;
        public readonly double SevenDaySnowAmount;
        public readonly double BaseDepth;
        
        public readonly int LiftsOpen;
        public readonly double AcresOpen;
        public readonly double PercentOpen;

        // true if just grabbed, false if from cache
        public readonly bool Refreshed;
        
        public Resort()
        {
            weatherReportSource = CachedResourceLoader.GetURL(ResortBaseUrl + WeatherReportPath, out bool weatherRefresh);
            liftStatusSource = CachedResourceLoader.GetURL(ResortBaseUrl + LiftAndTerrainStatusPath, out bool liftRefresh);

            Refreshed = weatherRefresh || liftRefresh;

            OvernightSnowfall = ExtractSingleValue(weatherReportSource, "OvernightSnowfall\":{\"Inches");
            OverallSnowConditions = ExtractSingleString(weatherReportSource, "OverallSnowConditions"); 
            LastUpdated = ExtractSingleString(weatherReportSource, "LastUpdatedText", ignoreComma:true).Replace("Updated ", ""); 
            
            TwentyFourHourSnowAmount = ExtractSingleValue(weatherReportSource, "TwentyFourHourSnowfall\":{\"Inches");
            FourtyEightHourSnowAmount = ExtractSingleValue(weatherReportSource, "FortyEightHourSnowfall\":{\"Inches");
            SevenDaySnowAmount = ExtractSingleValue(weatherReportSource, "SevenDaySnowfall\":{\"Inches");
            
            LiftsOpen = (int)ExtractSingleValue(liftStatusSource, "liftsOpen");
            BaseDepth = ExtractSingleValue(weatherReportSource, "BaseDepth\":{\"Inches");
            AcresOpen = ExtractSingleValue(liftStatusSource, "acresOpen");
            PercentOpen = ExtractSingleValue(liftStatusSource, "percentOpen");
        }

        public void PrintStatus()
        {
            ConsoleUtils.Print($"Resort Status as of {LastUpdated}");

            using (new ConsoleWriteIndent())
            {
                ConsoleUtils.Print($"Overall Snow Conditions: {OverallSnowConditions}");
            
                ConsoleUtils.Print($"Overnight Snow: {OvernightSnowfall}\", Base Depth: {BaseDepth}\"");
                
                ConsoleUtils.Print($"24 hour, 48 hour, 7 day snow totals: {TwentyFourHourSnowAmount}\", {FourtyEightHourSnowAmount}\", {SevenDaySnowAmount}\"");

                ConsoleUtils.Print($"Lifts open, Acres open, Percent open: {LiftsOpen}, {AcresOpen}, {PercentOpen}%");
            }
        }
        
        private double ExtractSingleValue(string source, string valueName)
        {
            string searchToken = "\"" + valueName + "\"";
            int startIndex = source.IndexOf(searchToken)+searchToken.Length;
            // stop collection after reaching a comma or a closing curly bracket
            int endIndex = Math.Min(source.IndexOf(",", startIndex), source.IndexOf("}", startIndex));

            // +-1 indexes account for the quotes around the desired value
            string result = source.Substring(startIndex+1, endIndex-startIndex-1);
            result = result.Replace("\"", "");
            
            // todo handle parse error
            return result == "null" ? 0 : Convert.ToDouble(result);
        }
        
        private string ExtractSingleString(string source, string valueName, bool ignoreComma = false)
        {
            string searchToken = "\"" + valueName + "\"";
            int startIndex = source.IndexOf(searchToken)+searchToken.Length;
            
            // stop collection after reaching a comma (maybe) or a closing curly bracket
            int endIndex;
            if (ignoreComma)
            {
                endIndex = source.IndexOf("}", startIndex);
            }
            else
            {
                endIndex = Math.Min(source.IndexOf(",", startIndex), source.IndexOf("}", startIndex));
            }
            
            // +-1 indexes account for the quotes around the desired value
            string result = source.Substring(startIndex+1, endIndex-startIndex-1);
            result = result.Replace("\"", "");
            
            // todo:  handle parse error
            // todo: reject strings too long
            return result;
        }
    }
}