﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventurePlanner.Gear;

public class GearLibrary
{
    protected List<GearItem> Library = new(); 
    
    public GearItem SelectOne(GearCategory category)
    {
        var options = Library.Where(item => item.Category == category);

        if (!options.Any())
        {
            Console.WriteLine("No " + category + " available in gear library");
            return null;
        }
            
        Console.WriteLine("Select a " + category);
        using (new ConsoleWriteIndent())
        {
            int i = 1;
            foreach (var item in options)
            {
                ConsoleUtils.Print(i+". " + item.Name);
                i++;
            }
        }

        int n = Int32.Parse(Console.ReadLine());

        return options.ToList()[n-1];
    }
}