﻿namespace AdventurePlanner.Gear;

public class GearItem
{
    public GearCategory Category;

    public string Name;
    
    /// <summary>
    /// Weight in grams
    /// </summary>
    public int Weight;
    
    // todo: add brand, image url, expiration date
}