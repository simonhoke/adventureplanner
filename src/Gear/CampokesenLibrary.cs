﻿namespace AdventurePlanner.Gear;

public class CampokesenLibrary : GearLibrary
{
    // todo: read library contents from a json file 
    
    public CampokesenLibrary()
    {
        Library.Add(new GearItem { Category = GearCategory.Backpack, Name = "Mountain Hardwear Alpine Light 35"});
        Library.Add(new GearItem { Category = GearCategory.ParkingPass, Name = "NW Forest Pass"});
        Library.Add(new GearItem { Category = GearCategory.ParkingPass, Name = "Discover Pass"});
    }
}