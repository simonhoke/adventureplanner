﻿namespace AdventurePlanner.Gear;

public enum GearCategory
{
    ParkingPass, // todo: connect to land owner map
    CampingPermit,
    ClimbingFee,
    ClimbingPermit,
    Vehicle, // todo: create subcategories: gas, tires, emergency gear, extra food, water, caffeine, contacts
    DownloadedMap,
    Backpack,
    Footwear, // approach shoe, climbing shoe, mountaineering boot, ski boot
    Pole,
    IceTool,
    UpperClothingLayer,
    LowerClothingLayer,
    Glove,
    Sock,
    Hat,
    Helmet,
    Rope,
    Harness,
    Protection,
    Gaiter,
    Crampon,
    Stove,
    Fuel,
    Utensils,
    Cookware, // pot, bowl, mug
    Lighter, // lighter
    Shelter, // tent, tarp, guylines, stakes
    SleepingPad,
    SleepingBag,
    Headlamp, // batteries
    Phone,
    Shovel,
    Probe,
    Beacon,
    Skis,
    Goggles,
    Skins,
    SkiStraps,
    SkiCrampons,
    SnowSaw,
    GlacierGlasses,
    Prescriptions,
    BlueBag,
    GarbageBag,
    FirstAid,
    RepairKit, // scraper, binding tool
    ToiletPaper,
    Toothbrush, // and toothpaste
    Sunscreen,
    Watch,
    Battery,
    Electrolytes,
    Water,
    Food,
    WaterBottle,
    WaterPurification,
}