using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventurePlanner
{
    public abstract class Activity
    {
        public readonly string Name;
        
        protected Activity(string name)
        {
            Name = name;
        }

        /// <summary>
        /// Reflection implementation from https://stackoverflow.com/a/5120714
        /// </summary>
        /// <returns></returns>
        public static List<Activity> LoadAllActivities()
        {
            var interfaceType = typeof(Activity);
            var allActivities = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(x => x.GetTypes())
                .Where(x => interfaceType.IsAssignableFrom(x) && !x.IsInterface && !x.IsAbstract)
                .Select(x => Activator.CreateInstance(x));

            return allActivities.Cast<Activity>().ToList();
        }
        
        /// <summary>
        /// Is this activity possible in the current weather conditions?
        /// May require user input
        /// </summary>
        /// <returns></returns>
        public abstract bool WeatherCheck(Conditions conditions, out string reason);

        // skill check

        // strength check

        // location check

        // gear check

        // partner check

        // cost check
    }
}