using System;
using AdventurePlanner.Gear;

namespace AdventurePlanner
{
    public class GearPlanner
    {
        public GearPlanner()
        {
            
        }

        private static string Prompt(string p)
        {
            Console.WriteLine(p + ": ");
            return Console.ReadLine();
        }

        public void Start()
        {
            Console.WriteLine("Gear Planner!");
            
            //var objective = Prompt("Objective Name");
            //var ascent = prompt("Ascent Route");
            //var descent = prompt("Descent Route");
            // date
            // weather
            // avy
            
            GearLibrary library = new CampokesenLibrary();

            foreach (GearCategory category in Enum.GetValues<GearCategory>())
            {
                library.SelectOne(category);
            }
            
            //var parkingPass = library.SelectOne(GearCategory.ParkingPass);
            
            //Console.WriteLine("Parking Pass Selected: " + parkingPass.Name);
            
            // todo: save plan as json
            
            // todo: html page that displays plan (with images)
        }
    }
}