using System;
using AdventurePlanner.Weather;

namespace AdventurePlanner.Activities
{
    public class IceClimb : Activity
    {
        public IceClimb() : base("Ice Climbing")
        {
            
        }
        
        public override bool WeatherCheck(Conditions conditions, out string reason)
        {
            double temp = conditions.CurrentWeather.GetTempF();

            if (temp > 29)
            {
                reason = Math.Round(temp, 0) + "F is too warm.";
                return false;
            }
            
            // TODO: also check season is not summer
            
            reason = Math.Round(temp, 0) + "F is cold enough.";
            return true;
        }
    }
}