namespace AdventurePlanner.Activities.Run
{
    public abstract class Run : Activity
    {
        protected Run(string name) : base(name)
        {

        }
        
        public override bool WeatherCheck(Conditions conditions, out string reason)
        {
            reason = "";
            return true;
        }
    }
}