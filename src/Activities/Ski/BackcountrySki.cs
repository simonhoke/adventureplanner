using AdventurePlanner.Avalanche;

namespace AdventurePlanner.Activities.Ski
{
    public class BackcountrySki : Ski
    {
        public BackcountrySki() : base("Backcountry Skiing")
        {
            
        }

        public override bool WeatherCheck(Conditions conditions, out string reason)
        {
            // TODO: fail if summer or fall

            reason = "Avalanche Forecast is " + conditions.AvalancheForecast.GetHighestDanger();
            
            // todo: use tomorrow's forecast as well
            if (conditions.AvalancheForecast.GetHighestDanger() >= DangerLevel.High)
            {
                return false;
            }
            
            return true;
        }
    }
}