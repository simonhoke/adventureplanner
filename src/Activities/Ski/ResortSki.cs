namespace AdventurePlanner.Activities.Ski
{
    public class ResortSki : Ski
    {
        public ResortSki() : base("Resort Skiing")
        {
            
        }

        public override bool WeatherCheck(Conditions conditions, out string reason)
        {
            reason = $"24 hour snow total: {conditions.Resort.TwentyFourHourSnowAmount}\"";
            
            return true; // because it's winter right now :p
        }
    }
}