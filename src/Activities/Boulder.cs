namespace AdventurePlanner.Activities
{
    public class Boulder : Activity
    {
        public Boulder() : base("Bouldering")
        {
            
        }

        public override bool WeatherCheck(Conditions conditions, out string reason)
        {
            // TODO: look at historical (last week) precip and sun
            
            bool dry = conditions.CurrentWeather.IsDry();
            reason = "It's dry enough, the weather is " + conditions.CurrentWeather.Desc[0].Description;
            
            if (!dry)
                reason = "It's " + conditions.CurrentWeather.Clouds.All + "% cloudy ";
            
            return dry;
        }
        
        // headlamp required if activity is afternoon
        // big lights required if activity time is after sunset 
        
        // gear required: no jackets if over 70
        // one jacket between 40 - 70
        // two jackets between 30 - 40
        // three jackets 15 - 30
    }
}