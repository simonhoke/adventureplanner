﻿using System.Net.Http;

namespace AdventurePlanner
{
    class AdventurePlanner
    {
        public static readonly HttpClient httpClient = new();
        
        static void Main(string[] args)
        {
            //new ActivityTypePlanner().Start();

            // specific route planner
            
            new GearPlanner().Start();
        }
    }
}
