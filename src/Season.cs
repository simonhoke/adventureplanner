using System;

namespace AdventurePlanner
{
    public class SeasonData
    {
        public static Season GetSeason(DateTime date) => date.Month switch
        {
            >= 1 and < 4 => Season.Winter,
            >= 4 and < 7 => Season.Spring,
            >= 7 and < 10 => Season.Summer,
            >= 10 and <= 12 => Season.Autumn,
            
            _ => throw new ArgumentOutOfRangeException(nameof(date), $"Date with unexpected month: {date.Month}."),
        };
    }
    
    public enum Season
    {
        Summer,
        Winter,
        Spring,
        Autumn,
    }
}