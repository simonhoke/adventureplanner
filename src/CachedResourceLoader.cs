using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace AdventurePlanner
{
    public class CachedResourceLoader
    {
        private static readonly string path = "cachedata/";

        private static readonly int CacheDurationMinutes = 30;

        static CachedResourceLoader()
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
        }
        
        /// <summary>
        /// Note that since special characters are stripped collisions may occur
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private static string GetFullPath(string url)
        {
            string cleanUrl = url.Replace("/", "").Replace(":", "");
            return path + cleanUrl + ".txt";
        }

        private static bool IsFresh(string url)
        {
            string fullPath = GetFullPath(url);
            var now = DateTime.Now;
            
            if (File.Exists(fullPath))
            {
                var mod = File.GetLastWriteTime(GetFullPath(url));
                return ((now - mod).TotalMinutes < CacheDurationMinutes);
            }
            
            return false;
        }
        
        public static string GetURL(string url, out bool refreshed)
        {
            if (IsFresh(url))
            {
                refreshed = false;
                return ReadFile(url);
            }
            else
            {
                refreshed = true;
                SaveUrlToFile(url);
                return ReadFile(url);
            }
        }

        private static void SaveUrlToFile(string url)
        {
            string result = AdventurePlanner.httpClient.GetStringAsync(url).Result;
            File.WriteAllText(GetFullPath(sha256(url)), result);
        }

        private static string ReadFile(string url)
        {
            var filename = sha256(url);
            return File.ReadAllText(GetFullPath(filename));
        }
        
        /// <summary>
        /// https://stackoverflow.com/a/14709940
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        static string sha256(string str)
        {
            var crypt = new SHA256Managed();
            string hash = String.Empty;
            byte[] crypto = crypt.ComputeHash(Encoding.ASCII.GetBytes(str));
            foreach (byte theByte in crypto)
            {
                hash += theByte.ToString("x2");
            }
            return hash;
        }
    }
}