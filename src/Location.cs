namespace AdventurePlanner
{
    public class Location
    {
        public readonly int OpenWeatherCityCode;
        
        public Location(string name)
        {
            if (name.Equals("Leavenworth"))
                OpenWeatherCityCode = 5800683;
        }
    }
}